# Beispielaufruf von puppet:
# puppet apply --modulepath=/home/jor/source/fablab-puppet/modules /home/jor/source/fablab-puppet/manifests/jochen-fablab-tools-site.pp
node default {
	include fablab_tools

	class { 'apt': 
		disable_keys => true,
	}
	
	import "/home/jor/source/fablab-puppet/modules/fablab_users/manifests/usershomeconfig.pp"

	@fablab_users::usershomeconfig::virtual { 'jor': }
		

	Fablab_users::Usershomeconfig::Virtual <| |>
}

