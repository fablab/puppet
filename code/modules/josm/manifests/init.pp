class josm {
	apt::key { "357FA575BC36BFB910E88579130A439C78FC0F87":
		source => "http://josm.openstreetmap.de/josm-apt.key",
	}

	apt::source { "josm":
		location        => "http://josm.openstreetmap.de/apt",
		release         => "$lsbdistcodename",
		repos           => "universe",
                include		=> { src => false },
		require		=> Apt::Key["357FA575BC36BFB910E88579130A439C78FC0F87"],
		notify  	=> Exec['apt_update'],
	}

	package { "josm":
		ensure          => present,
		require         => Apt::Source["josm"],
	}
}
