<?xml version="1.0" encoding="UTF-8"?>
<module fritzingVersion="0.7.5b.07.02.6130" moduleId="c3361082b5ff65e60744eb8a291da904">
    <author>John De Cristofaro</author>
    <title>ADS1115 16-bit ADC Breakout</title>
    <label>IC</label>
    <date>2012-11-15</date>
    <url>http://www.adafruit.com/products/1085</url>
    <tags>
        <tag>ADC</tag>
        <tag>breakout</tag>
        <tag>adafruit</tag>
        <tag>12</tag>
        <tag>bit</tag>
        <tag>pga</tag>
    </tags>
    <properties>
        <property name="family"></property>
        <property name="part number"></property>
    </properties>
    <description>ADS1015 12-bit I2C ADC+PGA Breakout</description>
    <views>
        <iconView>
            <layers image="icon/ADS1015_12_bit_ADC_Breakout__6e59a0fac02__icon__9d52212e768c8c91f7314a646b940696.svg">
                <layer layerId="icon"/>
            </layers>
        </iconView>
        <pcbView>
            <layers image="pcb/ADS1015_12_bit_ADC_Breakout__6e59a0fac02__pcb__e6f24069ac64ba2b52caa47eac9bb9a8.svg">
                <layer layerId="copper0"/>
                <layer layerId="copper1"/>
                <layer layerId="silkscreen"/>
            </layers>
        </pcbView>
        <breadboardView>
            <layers image="breadboard/ADS1115_16_bit_ADC_Breakout__ede16b30448__breadboard__4fc69d493aacaf136a9fd6215bcfd33f.svg">
                <layer layerId="breadboard"/>
            </layers>
        </breadboardView>
        <schematicView>
            <layers image="schematic/ADS1115_16_bit_ADC_Breakout__ede16b30448__schematic__317b3acbad95a4f44d7342c57dac727e.svg">
                <layer layerId="schematic"/>
            </layers>
        </schematicView>
    </views>
    <connectors>
        <connector id="connector3" type="male" name="SCL">
            <description>I2C clock</description>
            <views>
                <breadboardView>
                    <p layer="breadboard" svgId="connector3pin"/>
                </breadboardView>
                <schematicView>
                    <p layer="schematic" svgId="connector3pin"/>
                </schematicView>
                <pcbView>
                    <p layer="copper1" svgId="connector3pin"/>
                    <p layer="copper0" svgId="connector3pin"/>
                </pcbView>
            </views>
        </connector>
        <connector id="connector4" type="male" name="SDA">
            <description>I2C data</description>
            <views>
                <breadboardView>
                    <p layer="breadboard" svgId="connector4pin"/>
                </breadboardView>
                <schematicView>
                    <p layer="schematic" svgId="connector4pin"/>
                </schematicView>
                <pcbView>
                    <p layer="copper1" svgId="connector4pin"/>
                    <p layer="copper0" svgId="connector4pin"/>
                </pcbView>
            </views>
        </connector>
        <connector id="connector5" type="male" name="ADDR">
            <description>address</description>
            <views>
                <breadboardView>
                    <p layer="breadboard" svgId="connector5pin"/>
                </breadboardView>
                <schematicView>
                    <p layer="schematic" svgId="connector5pin"/>
                </schematicView>
                <pcbView>
                    <p layer="copper1" svgId="connector5pin"/>
                    <p layer="copper0" svgId="connector5pin"/>
                </pcbView>
            </views>
        </connector>
        <connector id="connector6" type="male" name="ALRT">
            <description>alert</description>
            <views>
                <breadboardView>
                    <p layer="breadboard" svgId="connector6pin"/>
                </breadboardView>
                <schematicView>
                    <p layer="schematic" svgId="connector6pin"/>
                </schematicView>
                <pcbView>
                    <p layer="copper1" svgId="connector6pin"/>
                    <p layer="copper0" svgId="connector6pin"/>
                </pcbView>
            </views>
        </connector>
        <connector id="connector7" type="male" name="A0">
            <description>address line 0</description>
            <views>
                <breadboardView>
                    <p layer="breadboard" svgId="connector7pin"/>
                </breadboardView>
                <schematicView>
                    <p layer="schematic" svgId="connector7pin"/>
                </schematicView>
                <pcbView>
                    <p layer="copper1" svgId="connector7pin"/>
                    <p layer="copper0" svgId="connector7pin"/>
                </pcbView>
            </views>
        </connector>
        <connector id="connector8" type="male" name="A1">
            <description>address line 1</description>
            <views>
                <breadboardView>
                    <p layer="breadboard" svgId="connector8pin"/>
                </breadboardView>
                <schematicView>
                    <p layer="schematic" svgId="connector8pin"/>
                </schematicView>
                <pcbView>
                    <p layer="copper1" svgId="connector8pin"/>
                    <p layer="copper0" svgId="connector8pin"/>
                </pcbView>
            </views>
        </connector>
        <connector id="connector9" type="male" name="A2">
            <description>address line 2</description>
            <views>
                <breadboardView>
                    <p layer="breadboard" svgId="connector9pin"/>
                </breadboardView>
                <schematicView>
                    <p layer="schematic" svgId="connector9pin"/>
                </schematicView>
                <pcbView>
                    <p layer="copper1" svgId="connector9pin"/>
                    <p layer="copper0" svgId="connector9pin"/>
                </pcbView>
            </views>
        </connector>
        <connector id="connector10" type="male" name="A3">
            <description>address line 3</description>
            <views>
                <breadboardView>
                    <p layer="breadboard" svgId="connector10pin"/>
                </breadboardView>
                <schematicView>
                    <p layer="schematic" svgId="connector10pin"/>
                </schematicView>
                <pcbView>
                    <p layer="copper1" svgId="connector10pin"/>
                    <p layer="copper0" svgId="connector10pin"/>
                </pcbView>
            </views>
        </connector>
        <connector id="connector1" type="male" name="VDD">
            <description>supply voltage</description>
            <views>
                <breadboardView>
                    <p layer="breadboard" svgId="connector1pin"/>
                </breadboardView>
                <schematicView>
                    <p layer="schematic" svgId="connector1pin"/>
                </schematicView>
                <pcbView>
                    <p layer="copper1" svgId="connector1pin"/>
                    <p layer="copper0" svgId="connector1pin"/>
                </pcbView>
            </views>
        </connector>
        <connector id="connector2" type="male" name="GND">
            <description>ground</description>
            <views>
                <breadboardView>
                    <p layer="breadboard" svgId="connector2pin"/>
                </breadboardView>
                <schematicView>
                    <p layer="schematic" svgId="connector2pin"/>
                </schematicView>
                <pcbView>
                    <p layer="copper1" svgId="connector2pin"/>
                    <p layer="copper0" svgId="connector2pin"/>
                </pcbView>
            </views>
        </connector>
    </connectors>
</module>
