class fablab_tools::inkscape_inkstitch (
	$version = '1.28.0',
) {
	include "fablab_tools::inkscape"

	exec { 'inkstitch_download':
		command => "/usr/bin/wget -q -c https://github.com/inkstitch/inkstitch/releases/download/v${version}/inkstitch-refs-tags-v${version}-linux-de_DE.zip -O /opt/inkstitch-${version}.zip",
		unless  => "/usr/bin/test -s /opt/inkstitch-${version}.zip",
		timeout => 600,
		require => Package['wget'],
		notify => Exec['inkstitch_deflate'],
	}

	exec { 'inkstitch_deflate':
		command     => "/usr/bin/unzip /opt/inkstitch-${version}.zip -d /usr/share/inkscape/extensions/",
		refreshonly => true,
		require     => [
			Exec["inkstitch_download"],
			Package['unzip'],
		],
	}
}
