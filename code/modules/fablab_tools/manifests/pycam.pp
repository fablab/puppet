class fablab_tools::pycam (
	$version = "0.5.1",
	$build = "1"
) {
	include "fablab_tools::opt"

	exec { "download-pycam":
		command => "/usr/bin/wget -q -c http://downloads.sourceforge.net/project/pycam/pycam/${version}/pycam_${version}-${build}_all.deb -O /opt/deb/pycam_${version}-${build}_all.deb",
		creates => "/opt/deb/pycam_${version}-${build}_all.deb",
		require => Package['wget'],
	}

	package { [ "python-gtkglext1", "python-support", "pstoedit" ]:
		ensure => present,
	}

	package { "pycam":
		provider => dpkg,
		ensure   => "latest",
		source   => "/opt/deb/pycam_${version}-${build}_all.deb",
		require  => [ Exec["download-pycam"], Package["python-gtkglext1", "python-support"] ],
	}

	file { "/opt/pycam-memory-leak.diff":
		ensure => present,
		source => "puppet:///modules/fablab_tools/pycam-memory-leak.diff",
	}

	exec { "patch-pycam":
		command => "/usr/bin/patch /usr/share/pyshared/pycam/Utils/threading.py < /opt/pycam-memory-leak.diff",
		onlyif => "/usr/bin/patch --force --dry /usr/share/pyshared/pycam/Utils/threading.py < /opt/pycam-memory-leak.diff",
		require => [ File["/opt/pycam-memory-leak.diff"], Package["pycam"] ],
	}
}
