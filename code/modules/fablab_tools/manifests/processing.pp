class fablab_tools::processing (
	$version = "3.5.3"
) {
	exec { "processing-download":
		command => "/usr/bin/wget -q -c http://download.processing.org/processing-${version}-linux64.tgz -O /opt/processing-${version}-linux64.tgz",
		creates => "/opt/processing-${version}-linux64.tgz",
		timeout => 900,
		require => Package['wget'],
	}

	exec { "/opt/processing-version":
		command => "/bin/tar -xvzf processing-${version}-linux64.tgz && chown -R 0.0 /opt/processing-${version}",
		creates => "/opt/processing-${version}",
		cwd => "/opt",
		require => Exec["processing-download"],
	}

	file { "/opt/processing":
		ensure => 'link',
		target => "/opt/processing-${version}",
		owner => 'root',
		group => 'root',
		require => Exec['/opt/processing-version'],
	}

	#exec { "/usr/bin/processing":
	#	command => "/bin/sed -e 's/APPDIR=`readlink -f \"\$0\"`//'g -e 's/`dirname \"\$APPDIR\"`/\\/opt\\/processing/'g /opt/processing/processing > /usr/bin/processing && chmod +x /usr/bin/processing",
	#	creates => "/usr/bin/processing",
	#	require => Exec["/opt/processing"],
	#}

	file { '/usr/bin/processing':
		ensure => 'link',
		target => '/opt/processing/processing',
		owner => 'root',
		group => 'root',
		require => File['/opt/processing'],
	}

	file { "/usr/share/applications/processing.desktop":
		ensure => present,
		source => "puppet:///modules/fablab_tools/processing.desktop",
	}
}
