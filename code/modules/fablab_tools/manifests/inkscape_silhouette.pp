class fablab_tools::inkscape_silhouette {
	include "fablab_tools::inkscape"

	if(!defined(Package["git"])) {
		package { "git":
			ensure => present,
		}
	}

	exec { "download-inkscape-silhouette":
		command => "/usr/bin/git clone https://github.com/fablabnbg/inkscape-silhouette.git /opt/inkscape-silhouette",
		creates => "/opt/inkscape-silhouette",
		require => Package["git"],
	}

	$packages = [
		'gettext',
		'python3-lxml',
		'python-setuptools',
		'python3-usb',
	]
	package { $packages: }

	exec { "install-inkscape-silhouette":
		command => "make install",
		cwd => "/opt/inkscape-silhouette",
		path => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games",
		creates => "/usr/share/inkscape/extensions/silhouette/Geometry.py",
		require => [
			Exec["download-inkscape-silhouette"],
			Package["gettext"],
			Package["python3-usb"],
			Package["python3-lxml"]
		],
	}

	file { '/etc/udev/rules.d/99-graphtec-silhouette.rules':
		ensure => 'file',
		source => 'puppet:///modules/fablab_tools/99-graphtec-silhouette.rules',
		owner => 'root',
		group => 'root',
		mode => '0644',
		notify => Exec['udev_trigger_silhouette'],
	}

	exec { 'udev_trigger_silhouette':
		command     => '/usr/bin/udevadm trigger',
		group       => 'root',
		user        => 'root',
		refreshonly => true,
	}
}
