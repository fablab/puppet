class fablab_tools::inkscape_embroidery {
	include "fablab_tools::inkscape"

	if(!defined(Package["git"])) {
		package { "git":
			ensure => present,
		}
	}

	exec { "download-inkscape_embroidery":
		command => "/usr/bin/git clone https://github.com/stesie/inkscape-embroidery.git /usr/src/inkscape-embroidery",
		creates => "/usr/src/inkscape-embroidery",
		require => Package["git"],
	}

	file { "/usr/share/inkscape/extensions/embroider.inx":
		ensure => present,
		source => "/usr/src/inkscape-embroidery/embroider.inx",
		require => Exec["download-inkscape_embroidery"],
	}

	file { "/usr/share/inkscape/extensions/embroider.py":
		ensure => present,
		source => "/usr/src/inkscape-embroidery/embroider.py",
		require => Exec["download-inkscape_embroidery"],
	}

	file { "/usr/share/inkscape/extensions/PyEmb.py":
		ensure => present,
		source => "/usr/src/inkscape-embroidery/PyEmb.py",
		require => Exec["download-inkscape_embroidery"],
	}
}
