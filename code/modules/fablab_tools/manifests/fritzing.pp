class fablab_tools::fritzing(
	$version = "CD-498", # 0.9.4
	$reference = "a1ffcea08814801903b1a9515b18cf97067968ae-master-498.bionic",
) {
	include "fablab_tools::opt"

	$pkgs = [
		'libqt5printsupport5',
		'libqt5serialport5',
		'libqt5sql5',
	]
    	package { $pkgs: ensure => "installed" }

	exec { "download-fritzing":
		creates => "/opt/fritzing-${version}.linux.AMD64.tar.bz2",
		command => "/usr/bin/wget -q -c -O '/opt/fritzing-${version}.linux.AMD64.tar.bz2' 'https://github.com/fritzing/fritzing-app/releases/download/${version}/fritzing-${reference}.linux.AMD64.tar.bz2'",
		timeout => 200,
		require => Package['wget'],
	}

	exec { "unpack-fritzing":
		creates => "/opt/fritzing-${reference}.linux.AMD64/",
		command => "/bin/tar xvjf '/opt/fritzing-${version}.linux.AMD64.tar.bz2'",
		cwd     => "/opt",
		require => Exec["download-fritzing"],
	}

	file { "/opt/fritzing-${reference}.linux.AMD64/":
		recurse => true,
		source  => "puppet:///modules/fablab_tools/Fritzing/",
		require => Exec["unpack-fritzing"],
		owner   => "root",
		group   => "root",
	}

	file { "/usr/local/bin/Fritzing":
		ensure  => symlink,
		target  => "/opt/fritzing-${reference}.linux.AMD64/Fritzing",
		require => Exec["unpack-fritzing"],
	}

	file { "/usr/share/applications/fritzing.desktop":
		ensure => present,
		source => "/opt/fritzing-${reference}.linux.AMD64/org.fritzing.Fritzing.desktop",
		require => Exec["unpack-fritzing"],
	}
}
