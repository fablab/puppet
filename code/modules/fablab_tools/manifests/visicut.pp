class fablab_tools::visicut(
	$version = "1.9-205-gd98e82d0+devel-1"
) {
	include "fablab_tools::opt"
	include "fablab_tools::potrace"
	include "fablab_tools::jre"

	package { "visicut":
		provider => dpkg,
		ensure   => latest,
		source => "/opt/deb/visicut_${version}_all.deb",
		require => [ Package[ "potrace", $fablab_tools::jre::jre_package ], Exec["visicut-download"] ],
	}
	exec { "visicut-download":
		command => "/usr/bin/wget -q -c https://fablab-rothenburg.de/wp-content/uploads/2023/11/visicut_${version}_all.deb -O /opt/deb/visicut_${version}_all.deb",
		creates => "/opt/deb/visicut_${version}_all.deb",
		require => Package['wget'],
	}

	file { "/usr/bin/VisiCut.Linux":
		ensure => link,
		target => "visicut",
		require => Package["visicut"],
		owner => "root",
		group => "root",
	}

	file { "/usr/share/inkscape/extensions/daemonize.py":
		ensure  => 'absent',
	}
}
