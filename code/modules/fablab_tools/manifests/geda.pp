class fablab_tools::geda {
    package { [
        'geda',
        'geda-utils',
        'gerbv',
        'pcb-gtk',
        'pcb-lesstif',
    ]:
        ensure => present,
    }
}
