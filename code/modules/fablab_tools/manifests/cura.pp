class fablab_tools::cura (
	$version = '5.4.0',
){
	package { 'cura':
		ensure => 'absent',
	}

	#apt::ppa { 'ppa:thopiekar/cura':
	#	ensure => 'absent',
	#}

	exec { 'cura-download':
		command => "/usr/bin/wget -q -c https://github.com/Ultimaker/Cura/releases/download/${version}/UltiMaker-Cura-${version}-linux-modern.AppImage -O /opt/Cura-${version}-modern.AppImage.tmp && mv /opt/Cura-${version}-modern.AppImage.tmp /opt/Cura-${version}-modern.AppImage",
		creates => "/opt/Cura-${version}-modern.AppImage",
		timeout => 1800,
		require => Package['wget'],
	}

	file { "/opt/Cura-${version}-modern.AppImage":
		owner   => 'root',
		group   => 'root',
		mode    => '0755',
		require => Exec['cura-download'],
	}

	file { '/usr/local/bin/cura':
		ensure  => 'symlink',
		target  => "/opt/Cura-${version}-modern.AppImage",
		require => File["/opt/Cura-${version}-modern.AppImage"],
	}

	file { '/usr/share/cura/':
		ensure  => 'directory',
		owner   => 'root',
		group   => 'root',
		mode    => '0755',
	}

	file { '/usr/share/cura/resources/':
		ensure  => 'directory',
		owner   => 'root',
		group   => 'root',
		mode    => '0755',
		require => File['/usr/share/cura'],
	}

	file { '/usr/share/cura/resources/images/':
		ensure  => 'directory',
		owner   => 'root',
		group   => 'root',
		mode    => '0755',
		require => File['/usr/share/cura/resources'],
	}

	file { '/usr/share/cura/resources/images/cura-icon.png':
		source  => 'puppet:///modules/fablab_tools/cura/cura-icon.png',
		require => File['/usr/share/cura/resources/images/'],
	}

	file { "/usr/share/applications/cura.desktop":
		source  => "puppet:///modules/fablab_tools/cura/cura.desktop",
		require => [
			File['/usr/local/bin/cura'],
			File['/usr/share/cura/resources/images/cura-icon.png'],
		],
	}

	file { '/usr/share/cura/resources/example':
		source  => 'puppet:///modules/fablab_tools/cura/example',
		recurse => true,
		owner   => 'root',
		group   => 'root',
		require => File['/usr/share/cura/resources'],
	}
}

