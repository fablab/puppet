class fablab_tools::inkscape {
	package { "inkscape":
		ensure => "installed"
	}

	file { "/usr/share/inkscape/extensions":
		source  => "puppet:///modules/fablab_tools/inkscape",
		recurse => true,
		owner   => "root",
		group   => "root",
		require => Package["inkscape"],
	}

	file { "/usr/share/inkscape/extensions/inkcutext1.inx":
		ensure => absent,
	}

	file { "/usr/share/inkscape/extensions/inkcut":
		ensure => absent,
		force  => true,
	}

	exec { "download-inkstitch":
		command => "/usr/bin/curl -sL -o/var/cache/apt/archives/inkstitch_3.0.1_amd64.deb https://github.com/inkstitch/inkstitch/releases/latest/download/inkstitch_3.0.1_amd64.deb",
		creates => "/var/cache/apt/archives/inkstitch_3.0.1_amd64.deb",
	}

	package {'inkstitch':
		provider => dpkg,
		source   => "/var/cache/apt/archives/inkstitch_3.0.1_amd64.deb",
		require  => Exec["download-inkstitch"],
	}
}
