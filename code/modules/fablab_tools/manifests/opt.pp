class fablab_tools::opt {
	file { "/opt/deb":
		ensure => directory,
		owner => "root",
		group => "root",
	}
}
