class fablab_tools::meshlab {
	package { "meshlab":
		ensure => "installed"
	}
}
