class fablab_tools::jre (
) {
	$jre_package = 'default-jre'

	package { [ $jre_package ]:
		ensure   => "installed",
	}

	package { [ "openjdk-7-jre", "openjdk-7-jre-headless" ]:
		ensure => absent,
	}
}
