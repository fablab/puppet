class fablab_tools::arduino (
	$version = '1.8.9',
){

	package { "arduino":
		ensure => "absent"
	}

	package { "arduino-core":
		ensure => "absent"
	}

	exec { "arduino-download":
		command => "/usr/bin/wget -q -c https://downloads.arduino.cc/arduino-${version}-linux64.tar.xz -O /opt/arduino-${version}-linux64.tar.xz",
		unless  => "/usr/bin/test -s /opt/arduino-${version}-linux64.tar.xz",
		timeout => 600,
		require => Package['wget'],
	}

	exec { "arduino-deflate":
		command => "/bin/tar -xf /opt/arduino-${version}-linux64.tar.xz",
		cwd	=> "/opt",
		unless  => "/usr/bin/test -d /opt/arduino-${version}",
		require => Exec["arduino-download"],
	}

	file { "/usr/share/applications/arduino.desktop":
		ensure => present,
		content => template("fablab_tools/arduino.desktop.erb"),
		require => Exec["arduino-deflate"],
	}

	file { "/usr/local/bin/arduino":
		ensure  => "symlink",
		target  => "/opt/arduino-${version}/arduino",
		require => Exec["arduino-deflate"],
	}

	file { "/opt/arduino-${version}/hardware/esp8266com":
		ensure	=> "directory",
		require => Exec["arduino-deflate"],
	}

	exec { "arduino-esp8266-download":
		path => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games",
		cwd	=> "/opt/arduino-${version}/hardware/esp8266com",
		command => "git clone https://github.com/esp8266/Arduino.git esp8266",
		unless  => "/usr/bin/test -d /opt/arduino-${version}/hardware/esp8266com/esp8266",
		timeout => 600,
		require	=> File["/opt/arduino-${version}/hardware/esp8266com"],
	}

	exec { "arduino-esp8266-get-bin-tools":
		path => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games",
		cwd	=> "/opt/arduino-${version}/hardware/esp8266com/esp8266/tools",
		command => "python get.py",
		unless  => "/usr/bin/test -d /opt/arduino-${version}/hardware/esp8266com/esp8266/tools/esptool",
		timeout => 600,
		require => Exec["arduino-esp8266-download"],
	}

}
