class fablab_tools::fabscan {
	file { "/opt/FabScanForProcessing":
		ensure  => present,
		source  => "puppet:///modules/fablab_tools/FabScanForProcessing",
		recurse => true,
	}

	file { "/usr/bin/FabScanForProcessing":
		ensure => symlink,
		target => "/opt/FabScanForProcessing/FabScanForProcessing",
	}

	file { "/usr/share/applications/FabScanForProcessing.desktop":
		ensure => present,
		source => "puppet:///modules/fablab_tools/FabScanForProcessing.desktop",
	}
}
