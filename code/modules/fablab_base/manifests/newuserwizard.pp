class fablab_base::newuserwizard {
	include "fablab_base::sudo"

	$hostname = $::fablab_base::puppetmaster_hostname

	file { "/opt/newuserwizard":
		ensure => present,
		content => template("fablab_base/newuserwizard.erb"),
		mode	=> '0755',
		owner	=> 'root',
	}

	file { "/opt/newuserwizard.priv":
		ensure => present,
		source => "puppet:///modules/fablab_base/newuserwizard.priv",
	}

	file { "/usr/share/applications/newuserwizard.desktop":
		ensure => present,
		source => "puppet:///modules/fablab_base/newuserwizard.desktop",
	}

	file_line { 'sudo_puppet':
		ensure  => present,
		path    => '/etc/sudoers',
		line    => 'ALL ALL=(ALL:ALL) NOPASSWD: /usr/bin/puppet agent -t',
		match   => 'ALL ALL=\(ALL:ALL\) NOPASSWD: /usr/bin/puppet agent -t',
		require => Package['sudo'],
	}
}
