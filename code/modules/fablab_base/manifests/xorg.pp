class fablab_base::xorg {
  fablab_base::remove_snap { 'firefox': }

	package { [
		"arctica-greeter",
		"arctica-greeter-guest-session",
		"aspell-de",
		"chromium-browser",
		"compizconfig-settings-manager",
    "firefox",
		"firefox-locale-de",
		"gimp",
		"gitk",
		"git-gui",
		"lightdm",
		"keepass2",
		"mono-complete",
		"scite",
		"tuxpaint",
		"ubuntu-desktop",
		"ubuntu-session",
		"x11vnc",
		"xsel",
		"xserver-xorg",
		"minetest",
		"minetest-mod-moreblocks",
		"minetest-mod-moreores",
		"minetest-mod-pipeworks",
		"minetest-mod-worldedit",
	]:
    ensure  => "installed",
    require => [
      Fablab_base::Remove_snap['firefox'],
    ],
	}

    package { [
		'gnome-initial-setup',
        'openbox',
        'unity-greeter',
    ]:
        ensure => 'purged',
    }

	service { 'lightdm':
		ensure  => 'running',
		require => [
			Package['lightdm'],
			Package['arctica-greeter'],
			Package['arctica-greeter-guest-session'],
		],
		provider => 'systemd',
	}

	service { 'gdm':
		ensure   => 'stopped',
		enable   => 'mask',
		provider => 'systemd',
	}

	file { '/etc/X11/Xsession.d/90disable-screensaver':
		ensure => present,
		source => 'puppet:///modules/fablab_base/90disable-screensaver',
	}

	file { '/etc/X11/default-display-manager':
		ensure => present,
		content => '/usr/sbin/lightdm',
		owner => 'root',
		group => 'root',
		mode => '0644',
	}

	file { '/etc/systemd/system/display-manager.service':
		ensure => 'link',
	        target => '/lib/systemd/system/lightdm.service',
		require => [
			Package['lightdm'],
		],
	}

  file { '/etc/dconf/profile/user':
    ensure => present,
    source => 'puppet:///modules/fablab_base/dconf-profile-user',
  }

  file { '/etc/dconf/db/local.d':
    ensure => 'directory',
  }

  file { '/etc/dconf/db/local.d/00-favorite-apps:':
    ensure => absent,
  }
  file { '/etc/dconf/db/local.d/00-favorite-apps':
    ensure  => present,
    source  => 'puppet:///modules/fablab_base/dconf-favorite-apps',
    require => File['/etc/dconf/db/local.d'],
    notify  => Exec['dconf_update'],
  }

  exec { 'dconf_update':
    command     => '/usr/bin/dconf update',
    refreshonly => true,
  }
}
