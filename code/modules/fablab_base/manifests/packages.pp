class fablab_base::packages {
	Package { ensure => "installed" }

	$utils = [
		'aptitude',
		'augeas-tools',
		'bind9-host',
		'bwm-ng',
		'exfat-fuse',
		'exfatprogs',
		'hexcurse',
		'htop',
		'iotop',
		'iptraf-ng',
		'iputils-ping',
		'libxml2-utils',
		'lzop',
		'mc',
		'mosh',
		'netcat-traditional',
		'nfs-common',
		'ntp',
		'pass',
		'pv',
		'pwgen',
		'scdaemon',
		'screen',
		'smartmontools',
		'socat',
		'strace',
		'tcpdump',
		'tig',
		'tmux',
		'tshark',
		'ubuntu-standard',
		'unzip',
		'uuid',
		'vim',
		'wget',
		'wipe',
		'whois',
		'xsltproc',
		'zsh',
	]
	package { $utils: }

	if defined(Package['curl']) == false {
		package { 'curl': ensure => present, }
	}

	if(!defined(Package["git"])) {
		package { "git":
			ensure => present,
		}
	}

	if(!defined(Package["build-essential"])) {
		package { "build-essential":
			ensure => present,
		}
	}

	package { "deja-dup":
        ensure => absent,
    }

        package { [ 'fonts-indic', 'fonts-beng', 'fonts-beng-extra', 'fonts-deva', 'fonts-deva-extra', 'fonts-gargi', 'fonts-knda', 'fonts-gubbi', 'fonts-gujr', 'fonts-gujr-extra', 'fonts-guru', 'fonts-guru-extra', 'fonts-kacst', 'fonts-kacst-one', 'fonts-kalapi', 'fonts-khmeros-core', 'fonts-lao', 'fonts-lklug-sinhala', 'fonts-lohit-beng-assamese', 'fonts-lohit-beng-bengali', 'fonts-lohit-deva', 'fonts-lohit-gujr', 'fonts-lohit-guru', 'fonts-lohit-knda', 'fonts-mlym', 'fonts-lohit-mlym', 'fonts-orya', 'fonts-lohit-orya', 'fonts-taml', 'fonts-lohit-taml', 'fonts-lohit-taml-classical', 'fonts-telu', 'fonts-lohit-telu', 'fonts-nakula', 'fonts-navilu', 'fonts-noto-cjk', 'fonts-orya-extra', 'fonts-pagul', 'fonts-sahadeva', 'fonts-samyak-deva', 'fonts-samyak-gujr', 'fonts-samyak-mlym', 'fonts-samyak-taml', 'fonts-sarai', 'fonts-sil-abyssinica', 'fonts-sil-padauk', 'fonts-smc', 'fonts-smc-anjalioldlipi', 'fonts-smc-chilanka', 'fonts-smc-dyuthi', 'fonts-smc-karumbi', 'fonts-smc-keraleeyam', 'fonts-smc-manjari', 'fonts-smc-meera', 'fonts-smc-rachana', 'fonts-smc-raghumalayalamsans', 'fonts-smc-suruma', 'fonts-smc-uroob', 'fonts-telu-extra', 'fonts-thai-tlwg', 'fonts-tibetan-machine', 'fonts-tlwg-garuda', 'fonts-tlwg-garuda-ttf', 'fonts-tlwg-kinnari', 'fonts-tlwg-kinnari-ttf', 'fonts-tlwg-laksaman', 'fonts-tlwg-laksaman-ttf', 'fonts-tlwg-loma', 'fonts-tlwg-loma-ttf', 'fonts-tlwg-mono', 'fonts-tlwg-mono-ttf', 'fonts-tlwg-norasi', 'fonts-tlwg-norasi-ttf', 'fonts-tlwg-purisa', 'fonts-tlwg-purisa-ttf', 'fonts-tlwg-sawasdee', 'fonts-tlwg-sawasdee-ttf', 'fonts-tlwg-typewriter', 'fonts-tlwg-typewriter-ttf', 'fonts-tlwg-typist', 'fonts-tlwg-typist-ttf', 'fonts-tlwg-typo', 'fonts-tlwg-typo-ttf', 'fonts-tlwg-umpush', 'fonts-tlwg-umpush-ttf', 'fonts-tlwg-waree', 'fonts-tlwg-waree-ttf', 'fonts-noto-unhinted', 'fonts-noto-hinted', 'fonts-noto', 'josm', 'fonts-noto-ui-extra', 'fonts-noto-extra', 'fonts-noto-core', 'fonts-noto-ui-core' ]:
                ensure => absent,
        }

}
