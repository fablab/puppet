# Remove a snap package
define fablab_base::remove_snap ($package = $name) {
    exec { "remove_snap_${package}":
        command => "/usr/bin/snap remove ${package}",
        user    => 'root',
        group   => 'root',
        onlyif  => "/usr/bin/snap list ${package}",
    }
}
