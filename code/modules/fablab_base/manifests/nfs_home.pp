class fablab_base::nfs_home (
	$ensure = "present",
	$mountpoint = "/home",
) {
	mount { $mountpoint:
		device  => "192.168.111.1:/nfs/fabhome",
		fstype  => "nfs",
		ensure  => $ensure ? {
			"present" => "mounted",
			"absent"  => "absent",
		},
		options => "defaults,noatime,nfsvers=3",
		atboot  => "true",
		require => Package['nfs-common'],
	}
}
