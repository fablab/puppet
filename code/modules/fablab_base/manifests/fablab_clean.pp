# fablab_base::fablab_clean
#
# Puppet Klasse, die einen System-Service installiert,
# der bei jedem Systemstart (bevor andere Services gestartet
# werden), versucht einen möglichst sauberen Zustand
# herzustellen, sodass insb. puppet auf jeden Fall lauffähig
# ist (und damit in der Lage, den Rest des Systems immer
# wieder glatt zu ziehen).
#
class fablab_base::fablab_clean {
	file { '/usr/local/sbin/fablab-clean':
		ensure  => present,
		source  => 'puppet:///modules/fablab_base/fablab-clean',
		mode    => '0755',
		owner   => 'root',
		group   => 'root',
	}

	file { '/lib/systemd/system/fablab-clean.service':
		ensure  => present,
		source  => 'puppet:///modules/fablab_base/fablab-clean.service',
		mode    => '0644',
		owner   => 'root',
		group   => 'root',
	}

	service { 'fablab-clean':
		enable   => true,
		provider => systemd,
		require  => File[ '/usr/local/sbin/fablab-clean', '/lib/systemd/system/fablab-clean.service' ],
	}
}
