class fablab_base::nodejs {
	package { [ 'nodejs', 'npm' ]:
		ensure => installed
	}
}
