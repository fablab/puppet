class fablab_base (
	$puppetmaster_hostname
) {
	include "fablab_base::ubuntu_sourcelist"
	include "fablab_base::unattended_upgrade"
	include "fablab_base::gnome_terminal"
	include "fablab_base::packages"
	include "fablab_base::xorg"
	include "fablab_base::nodejs"
	include "fablab_base::fablab_clean"
	include "fablab_base::newuserwizard"
	include "fablab_base::visicutpublisher"
	include "fablab_base::apport"
	include "fablab_base::sudo"

	class { 'apt':
		purge => {
			'sources.list'   => true,
			'sources.list.d' => true,
		}
	}

	file { "/etc/apt/apt.conf.d/99auth":       
		owner     => root,
		group     => root,
		content   => "APT::Get::AllowUnauthenticated yes;",
		mode      => '644'
	}

	Class['apt::update'] -> Package <| provider == 'apt' |>

	file { '/etc/sysctl.d/10-magic-sysrq.conf':
		content	=> 'kernel.sysrq = 1',
		owner     => root,
		group     => root,
		mode      => '644'
	}


	#
	# IPv4 vor IPv6 (weil Tunnel und so)
	#
	file_line { 'gai':
		ensure => present,
		path   => '/etc/gai.conf',
		line   => 'precedence ::ffff:0:0/96  100',
		match  => 'precedence ::ffff:0:0/96  100',
	}




	file { '/etc/udev/rules.d/99-yubikey.rules':
		ensure => 'file',
		source => 'puppet:///modules/fablab_base/99-yubikey.rules',
		notify => Exec['udev_trigger'],
	}

	exec { 'udev_trigger':
		command     => '/usr/bin/udevadm trigger',
		refreshonly => true,
	}
}
