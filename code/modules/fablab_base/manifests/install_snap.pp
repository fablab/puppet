# Install a snap package
define fablab_base::install_snap ($package = $name, $options = '') {
    exec { "install_snap_${package}":
        command => "/usr/bin/snap install ${package} ${options}",
        user    => 'root',
        group   => 'root',
        unless  => "/usr/bin/snap list ${package}",
    }
}
