class fablab_base::gnome_terminal {
	package { 'gnome-terminal':
		ensure => present,
	}

	file { '/etc/alternatives/x-terminal-emulator':
		ensure  => "link",
		group   => "root",
		owner   => "root",
		target  => "/usr/bin/gnome-terminal.wrapper",
		require => Package['gnome-terminal'],
	}
}
