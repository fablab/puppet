class fablab_base::sudo {
	package { 'sudo':
		ensure => installed,
	}
}
