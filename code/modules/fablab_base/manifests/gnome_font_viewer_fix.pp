class fablab_base::gnome_font_viewer_fix {
	package { 'gnome-font-viewer':
		ensure => installed,
	}

	file_line { 'gnome-font-viewer-no-dbus':
		ensure  => absent,
		path    => '/usr/share/applications/org.gnome.font-viewer.desktop',
		line    => 'DBusActivatable=true',
		require => Package['gnome-font-viewer'],
	}
}
