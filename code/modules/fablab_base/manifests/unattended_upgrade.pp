class fablab_base::unattended_upgrade {
	package { "unattended-upgrades":
		ensure => present,
	}

	file { "/etc/apt/apt.conf.d/10periodic":
		ensure => "present",
		require => Package["unattended-upgrades"],
		content => 'APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "35";
APT::Periodic::Unattended-Upgrade "1";
APT::Periodic::RandomSleep "0";
',
		owner => "root",
		group => "root",
	}

	file { "/etc/apt/apt.conf.d/50unattended-upgrades":
		ensure => "present",
		source => "puppet:///modules/fablab_base/50unattended-upgrades",
		mode   => "0644",
		owner  => "root",
		group  => "root",
	}

	file { "/etc/apt/apt.conf.d/50unattended-upgrades.ucf-dist":
		ensure => "absent",
	}

	cron { "unattended-upgrade":
		name => "unattended-upgrade",
		ensure => "present",
		environment => 'PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"',
		command => "/usr/bin/unattended-upgrade",
		minute => "*/30",
		user => "root",
		require => Package["unattended-upgrades"],
	}
}

