class fablab_base::ubuntu_sourcelist ($version = $lsbdistcodename) {
        apt::source { "ubuntu_archiv_$version":
                location        => "http://archive.ubuntu.com/ubuntu/",
                release         => "$version",
                repos           => "main restricted universe multiverse",
                include		=> { src => false },
		notify  	=> Exec['apt_update'],
        }

        apt::source { "ubuntu_archiv_$version-updates":
                location        => "http://archive.ubuntu.com/ubuntu/",
                release         => "$version-updates",
                repos           => "main restricted universe multiverse",
                include		=> { src => false },
		notify  	=> Exec['apt_update'],
        }

        apt::source { "ubuntu_archiv_$version-security":
                location        => "http://archive.ubuntu.com/ubuntu/",
                release         => "$version-security",
                repos           => "main restricted universe multiverse",
                include		=> { src => false },
		notify  	=> Exec['apt_update'],
        }

        apt::source { "canonical_archiv_$version":
                location        => "http://archive.canonical.com/ubuntu/",
                release         => "$version",
                repos           => "partner",
                include		=> { src => false },
		notify  	=> Exec['apt_update'],
        }

    apt::ppa { "ppa:mozillateam/ppa": }
    file { "/etc/apt/preferences.d/mozilla-firefox":
        ensure => present,
        source => "puppet:///modules/fablab_base/apt-preferences-mozilla-firefox",
    }
}

