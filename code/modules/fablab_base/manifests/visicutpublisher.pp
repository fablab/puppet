class fablab_base::visicutpublisher {
	$hostname = $::fablab_base::puppetmaster_hostname

	file { "/opt/visicutpublisher":
		ensure => present,
		content => template("fablab_base/visicutpublisher.erb"),
		mode	=> '0755',
		owner	=> 'root',
	}

	file { "/opt/visicutpublisher.priv":
		ensure => present,
		source => "puppet:///modules/fablab_base/visicutpublisher.priv",
	}
}

