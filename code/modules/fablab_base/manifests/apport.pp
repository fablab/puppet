class fablab_base::apport {

	file { '/etc/default/apport':
		content => "enabled=0\n",
		owner	=> 'root',
		group	=> 'root',
		mode	=> '0644',
	}
}

