class fablab_base::lxde {
	package { "lxde":
		ensure => present,
	}

	package { "lxpanel-indicator-applet-plugin":
		ensure => present,
	}

	package { "lxsession-default-apps":
		ensure => present,
	}

	package { "lxsession-logout":
		ensure => present,
	}

	package { "lxshortcut":
		ensure => present,
	}

	file { "/etc/lightdm/lightdm.conf":
		content => "[SeatDefaults]\nuser-session=LXDE",
		require => Package["lightdm"],
		notify	=> Service["lightdm"],
       }
}
