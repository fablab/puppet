class fablab_base::nfs_share (
	$ensure = "present",
	$nfs_server_ipaddr
) {
	file { "/nfs":
		ensure => "directory",
	}

	file { "/nfs/fabhome":
		ensure => "directory",
	}

	mount { "/nfs/fabhome":
		device  => "${nfs_server_ipaddr}:/nfs/fabhome",
		fstype  => "nfs",
		ensure  => $ensure ? {
			"present" => "mounted",
			"absent"  => "absent",
		},
		options => "defaults,noatime,nfsvers=3,_netdev",
		atboot  => "true",
		require => [ Package['nfs-common'], File["/nfs/fabhome"] ],
	}
}
