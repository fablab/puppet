class fablab_games{
	file { '/usr/local/bin/install-minecraft.sh':
		ensure  => present,
		source  => 'puppet:///modules/fablab_games/install-minecraft.sh',
		mode    => '0755',
		owner   => 'root',
		group   => 'root',
	}
} 
