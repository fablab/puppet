#! /bin/sh

adduser $USER lp
adduser $USER dialout
adduser $USER audio
adduser $USER video
adduser $USER plugdev

echo "[Desktop Entry]\nVersion=1.0\nType=Link\nComment=Verzeichnis für dauerhafte Daten\nURL=file:///nfs/fabhome/guest-data\nIcon=folder\n" > $HOME/dauerhaft.desktop
echo "[Desktop Entry]\nVersion=1.0\nType=Link\nComment=Gemeinsamer Ordner\nURL=file:///nfs/fabhome/share4all\nIcon=folder\n" > $HOME/Gemeinsamer\ Ordner.desktop

