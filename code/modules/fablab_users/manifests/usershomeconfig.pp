define fablab_users::usershomeconfig::virtual {
	$visicut_job_prefix = $title

	file { "/home/${title}/.visicut":
		source  => "puppet:///modules/fablab_users/visicut",
		owner   => $title,
		group   => $title,
		mode    => '0755',
		recurse => true,
	}

	file { "/home/${title}/.visicut/devices/Atom_32_Schneider.xml":
		ensure => absent,
	}

	file { "/home/${title}/.visicut/devices/Epilog_32_ZING.xml":
		content => template("fablab_users/Epilog_32_ZING.xml.erb"),
		owner   => $title,
		group   => $title,
	}

	file { "/home/${title}/Zentrale Ablage":
		ensure => symlink,
		target => "/nfs/fabhome/${title}",
	}
	file { "/home/${title}/Gemeinsamer Ordner":
		ensure => symlink,
		target => "/nfs/fabhome/share4all",
	}
}
