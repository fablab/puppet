class fablab_users::guest_sessions {
	$visicut_job_prefix = 'guest'

	file { "/var/guest-data":
		ensure => "directory",
		owner => "root",
		group => "root",
	}

	mount { "/var/guest-data":
		device => "/home",
		fstype => "none",
		ensure => "mounted",
		options => "rw,bind",
		require => File["/var/guest-data"],
	}

	file { "/etc/skel/.visicut":
		source  => "puppet:///modules/fablab_users/visicut",
		owner   =>  'root',
		mode    =>  '0755',
		recurse => true,
	}

	file { "/etc/skel/.visicut/devices/Epilog_32_ZING.xml":
		content => template("fablab_users/Epilog_32_ZING.xml.erb"),
		owner => "root",
		group => "root",
		require => File["/etc/skel/.visicut"],
	}

	file { "/etc/arctica-greeter":
		ensure => "directory",
		owner => "root",
		group => "root",
	}		

	file { "/etc/arctica-greeter/guest-session":
		ensure => "directory",
		owner => "root",
		group => "root",
	}		

	file { "/etc/arctica-greeter/guest-session/prefs.sh":
		ensure => "present",
		source => 'puppet:///modules/fablab_users/guest-session/prefs.sh',
		owner => "root",
		group => "root",
	}

	file_line { "guest-access-run-lock":
		path => "/etc/apparmor.d/abstractions/lightdm",
		line => "  /run/lock/** rw,",
		require => Package['lightdm'],
		notify => Exec['apparmor-reload'],
	}

	file_line { "guest-access-nfs":
		path => "/etc/apparmor.d/abstractions/lightdm",
		line => "  /nfs/ r,",
		require => Package['lightdm'],
		notify => Exec['apparmor-reload'],
	}

	file_line { "guest-access-nfs-fabhome":
		path => "/etc/apparmor.d/abstractions/lightdm",
		line => "  /nfs/fabhome/ r,",
		require => Package['lightdm'],
		notify => Exec['apparmor-reload'],
	}

	file_line { "guest-access-nfs-fabhome-guest-data":
		path => "/etc/apparmor.d/abstractions/lightdm",
		line => "  /nfs/fabhome/guest-data/ r,",
		require => Package['lightdm'],
		notify => Exec['apparmor-reload'],
	}

	file_line { "guest-access-nfs-fabhome-guest-data-rw":
		path => "/etc/apparmor.d/abstractions/lightdm",
		line => "  /nfs/fabhome/guest-data/** rw,",
		require => Package['lightdm'],
		notify => Exec['apparmor-reload'],
	}

	file_line { "guest-access-nfs-fabhome-share4all":
		path => "/etc/apparmor.d/abstractions/lightdm",
		line => "  /nfs/fabhome/share4all/ r,",
		require => Package['lightdm'],
		notify => Exec['apparmor-reload'],
	}

	file_line { "guest-access-nfs-fabhome-share4all-r":
		path => "/etc/apparmor.d/abstractions/lightdm",
		line => "  /nfs/fabhome/share4all/** r,",
		require => Package['lightdm'],
		notify => Exec['apparmor-reload'],
	}

	file_line { "guest-access-media":
		path => "/etc/apparmor.d/abstractions/lightdm",
		line => "  /media/ r,",
		require => Package['lightdm'],
		notify => Exec['apparmor-reload'],
	}

	file_line { "guest-access-media-rw":
		path => "/etc/apparmor.d/abstractions/lightdm",
		line => "  /media/** rmwlixk,",
		require => Package['lightdm'],
		notify => Exec['apparmor-reload'],
	}

	file { "/etc/apparmor.d/lightdm-guest-session":
                ensure  => 'file',
		source  => "puppet:///modules/fablab_users/guest-session/apparmor-profile",
		owner   => 'root',
		mode    => '0644',
		require => Package['lightdm'],
		notify  => Exec['apparmor-reload'],
	}

	file_line { "fusermount_ux":
		path => '/etc/apparmor.d/abstractions/lightdm',
		line => '  /bin/fusermount ux,',
		match => '\ \ /bin/fusermount\ ',
		require => Package['lightdm'],
		notify => Exec['apparmor-reload'],
	}

	exec { "apparmor-reload":
		command => "/usr/sbin/apparmor_parser -r /etc/apparmor.d/lightdm-guest-session",
		refreshonly => true,
		require => Package['lightdm'],
	}
}
