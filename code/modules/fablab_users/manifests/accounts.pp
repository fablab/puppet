define fablab_users::accounts::virtual (
	$ensure = "present",
	$uid,
	$realname,
	$pass
) {

  if $ensure == "absent" {
    user { $title:
      ensure            => 'absent',
    }

    group { $title:
      ensure            => 'absent',
      require           => User[$title],
    }
   
    file { "/home/${title}":
      ensure            => 'absent',
      force             => true,
    }
  }
  else {
   
    user { $title:
      ensure            =>  'present',
      uid               =>  $uid,
      gid               =>  $title,
      shell             =>  '/bin/bash',
      home              =>  "/home/${title}",
      comment           =>  $realname,
      groups            => [ 'sudo', 'lp', 'video', 'audio', 'plugdev', 'dialout' ],
      password          =>  $pass,
      managehome        =>  true,
      require           =>  Group[$title],
    }
   
    group { $title:
      gid               =>  $uid,
    }
   
    file { "/home/${title}":
      ensure            =>  directory,
      owner             =>  $title,
      group             =>  $title,
      mode              =>  '0755',
      require           =>  [ User[$title], Group[$title] ],
    }

    @fablab_users::usershomeconfig::virtual { $title: }

  }
}

