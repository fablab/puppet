class fablab_users::users {
	include fablab_users::guest_sessions

	lookup('fablab_users', {merge => 'hash'}).each |String $username, Hash $userdata| {
		@fablab_users::accounts::virtual { $username:
			uid      => $userdata['uid'],
			realname => $userdata['realname'],
			pass     => $userdata['password_hash'],
		}
	}

	group { "fablab":
		ensure => 'present',
		gid    => '10000',
	}

	Fablab_users::Accounts::Virtual <| |>
	Fablab_users::Usershomeconfig::Virtual <| |>
}
